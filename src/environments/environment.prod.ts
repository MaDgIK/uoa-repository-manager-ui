export const environment = {
  production: true,
  AAI_LOGOUT: 'https://aai.openaire.eu/proxy/saml2/idp/SingleLogoutService.php?ReturnTo=',
  MATOMO_URL: 'https://analytics.openaire.eu/',
  MATOMO_SITE: 111,
  API_ENDPOINT: '/api',
  FAQ_ENDPOINT: '/uoa-admin-tools/api',
  FAQ_HOMEPAGE: '/uoa-admin-tools/dashboard'
};
