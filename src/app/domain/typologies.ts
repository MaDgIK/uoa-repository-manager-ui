export const typologies = [
  {
    value: '',
    name: '[Other] (enter name below)'
  },
  {
    value: 'Avesis',
    name: 'Avesis'
  },
  {
    value: 'CKAN',
    name: 'CKAN'
  },
  {
    value: 'CONTENTdm',
    name: 'CONTENTdm'
  },
  {
    value: 'Converis',
    name: 'Converis'
  },
  {
    value: 'CRIStin',
    name: 'CRIStin'
  },
  {
    value: 'DataVerse',
    name: 'DataVerse'
  },
  {
    value: 'Dialnet',
    name: 'Dialnet CRIS'
  },
  {
    value: 'Digibib',
    name: 'Digibib'
  },
  {
    value: 'Digital Commons',
    name: 'Digital Commons'
  },
  {
    value: 'DigiTool',
    name: 'DigiTool'
  },
  {
    value: 'DIVA',
    name: 'DIVA'
  },
  {
    value: 'Diva-Portal',
    name: 'Diva-Portal'
  },
  {
    value: 'dLibra',
    name: 'dLibra'
  },
  {
    value: 'Doks',
    name: 'Doks'
  },
  {
    value: 'DPubS',
    name: 'DPubS'
  },
  {
    value: 'Drupal',
    name: 'Drupal'
  },
  {
    value: 'DSpace',
    name: 'DSpace'
  },
  {
    value: 'DSpace-CRIS',
    name: 'DSpace-CRIS'
  },
  {
    value: 'E - Journal',
    name: 'E - Journal'
  },
  {
    value: 'Earmas',
    name: 'Earmas'
  },
  {
    value: 'EPrints',
    name: 'EPrints'
  },
  {
    value: 'ePubTk',
    name: 'ePubTk'
  },
  {
    value: 'eSciDoc',
    name: 'eSciDoc'
  },
  {
    value: 'ETD',
    name: 'ETD'
  },
  {
    value: 'ETD - db',
    name: 'ETD - db'
  },
  {
    value: 'Fedora',
    name: 'Fedora'
  },
  {
    value: 'Fez',
    name: 'Fez'
  },
  {
    value: 'GAPworks',
    name: 'GAPworks'
  },
  {
    value: 'Greenstone',
    name: 'Greenstone'
  },
  {
    value: 'HAL',
    name: 'HAL'
  },
  {
    value: 'Haplo',
    name: 'Haplo'
  },
  {
    value: 'HyperJournal',
    name: 'HyperJournal'
  },
  {
    value: 'invenio',
    name: 'invenio'
  },
  {
    value: 'IRIS',
    name: 'IRIS'
  },
  {
    value: 'Metis',
    name: 'Metis'
  },
  {
    value: 'MyCoRe',
    name: 'MyCoRe'
  },
  {
    value: 'MySQL',
    name: 'MySQL'
  },
  {
    value: 'Nesstar',
    name: 'Nesstar'
  },
  {
    value: 'OJS',
    name: 'OJS'
  },
  {
    value: 'Omega',
    name: 'Omega-PSIR'
  },
  {
    value: 'Open Repository',
    name: 'Open Repository'
  },
  {
    value: 'OPUS',
    name: 'OPUS'
  },
  {
    value: 'Pica - Verbundkatalog',
    name: 'Pica - Verbundkatalog'
  },
  {
    value: 'Proprietary Software',
    name: 'Proprietary Software'
  },
  {
    value: 'PUMA',
    name: 'PUMA'
  },
  {
    value: 'PURE',
    name: 'PURE'
  },
  {
    value: 'SciELO',
    name: 'SciELO'
  },
  {
    value: 'SFIX',
    name: 'SFIX'
  },
  {
    value: 'SIGMA',
    name: 'SIGMA Research'
  },
  {
    value: 'SoleCRIS',
    name: 'SoleCRIS'
  },
  {
    value: 'Symplectic',
    name: 'Symplectic Elements'
  },
  {
    value: 'VITAL',
    name: 'VITAL'
  },
  {
    value: 'VIVO',
    name: 'VIVO'
  },
  {
    value: 'VTOAI',
    name: 'VTOAI'
  },
  {
    value: 'WEKO',
    name: 'WEKO'
  },
  {
    value: 'Worktribe',
    name: 'Worktribe'
  },
  {
    value: 'XooNIps',
    name: 'XooNIps'
  },
];
