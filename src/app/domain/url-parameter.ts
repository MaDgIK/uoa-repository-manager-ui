/**
 * Created by stefania on 9/6/16.
 */
export class URLParameter {
    key: string;
    value: string[];
}
