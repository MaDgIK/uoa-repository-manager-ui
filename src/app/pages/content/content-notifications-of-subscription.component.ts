import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BrokerService } from '../../services/broker.service';
import { loadingEvents, noEventsForTopic, noServiceMessage } from '../../domain/shared-messages';
import { EventsPage } from '../../domain/typeScriptClasses';

@Component ({
  selector: 'app-content-notifications-of-subscription',
  templateUrl: 'content-notifications-of-subscription.component.html'
})

export class ContentNotificationsOfSubscriptionComponent implements OnInit {
  noEvents: string;
  errorMessage: string;
  loadingMessage: string;

  subId: string;
  topic: string;
  lastTopicEntry = '';
  eventsPage: EventsPage;
  currentPage: number;  /* DELETE WHEN getNotificationsBySubscriptionId IS FIXED AND SENDS CORRECT VALUE FOR CURRENT PAGE */

  selectedItemIndex: number;

  constructor(private route: ActivatedRoute,
              private brokerService: BrokerService) {}

  ngOnInit () {
    this.subId = this.route.snapshot.paramMap.get('id');
    this.currentPage = 0; /* DELETE WHEN getNotificationsBySubscriptionId IS FIXED AND SENDS CORRECT VALUE FOR CURRENT PAGE */
    this.getEventsPage(0);
    let body = document.getElementsByTagName('body')[0];
    body.classList.remove("top_bar_active");   //remove the class
    body.classList.remove("page_heading_active");
    body.classList.remove("landing");
    body.classList.add("dashboard");
  }

  getEventsPage(page: number) {
    this.noEvents = '';
    this.errorMessage = '';
    this. loadingMessage = loadingEvents;
    this.brokerService.getNotificationsBySubscriptionId(this.subId, page, 10).subscribe(
      events => this.eventsPage = events,
      error => {
        this.loadingMessage = '';
        this.errorMessage = noServiceMessage;
        console.log(error);
      },
      () => {
        this.loadingMessage = '';
        console.log(this.eventsPage);
        if (!this.eventsPage.total) {
          this.noEvents = noEventsForTopic;
        }
        this.getCorrectTopic();
        // console.log('Topic: ' + this.topic);
        this.lastTopicEntry = this.topic.substring(this.topic.lastIndexOf('|') + 1).toLowerCase();
        this.lastTopicEntry = this.replaceAll(this.lastTopicEntry, '_', ' ');
        // console.log('Last topic entry: ' + this.lastTopicEntry);
      }
    );
  }

  replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }


  goToNextPage() {
    /* RESTORE WHEN getNotificationsBySubscriptionId IS FIXED AND SENDS CORRECT VALUE FOR CURRENT PAGE */
    /*if(this.eventsPage.currPage < this.eventsPage.totalPages) {
      console.log(`Get me page ${this.eventsPage.currPage+1}!`);
      this.getEventsPage(this.eventsPage.currPage+1);
    }*/

    /* DELETE WHEN getNotificationsBySubscriptionId IS FIXED AND SENDS CORRECT VALUE FOR CURRENT PAGE */
    if ( (this.currentPage + 1) < this.eventsPage.totalPages) {
      this.currentPage = this.currentPage + 1;
      console.log(`Get me page ${this.currentPage}!`);
      this.getEventsPage(this.currentPage);

      window.scrollTo(0, 0);
    }
  }

  goToPreviousPage() {
    /* RESTORE WHEN getNotificationsBySubscriptionId IS FIXED AND SENDS CORRECT VALUE FOR CURRENT PAGE */
    /*if(this.eventsPage.currPage > 0) {
      console.log(`Get me page ${this.eventsPage.currPage-1}!`);
      this.getEventsPage(this.eventsPage.currPage-1);
    }*/

    /* DELETE WHEN getNotificationsBySubscriptionId IS FIXED AND SENDS CORRECT VALUE FOR CURRENT PAGE */
    if (this.currentPage > 0) {
      this.currentPage = this.currentPage - 1;
      console.log(`Get me page ${this.currentPage}!`);
      this.getEventsPage(this.currentPage);

      window.scrollTo(0, 0);
    }
  }


  isHighlighted(item: any, itemList: any[]) {
    return itemList.some(x => x === item);
  }

  getCorrectTopic() {
    const temp = this.eventsPage.topic.split('/');
    this.topic = temp[0];
    for (let i = 1; i < temp.length; i++) {
      this.topic += ` | ${temp[i]}`;
    }
  }

  displayFullResultInfo(i: number) {
    if (this.selectedItemIndex === i) {
      this.selectedItemIndex = null;
    } else {
      this.selectedItemIndex = i;
    }
  }

  showMore(i: number) {
    this.selectedItemIndex = i;
  }

  showLess(i: number) {
    this.selectedItemIndex = null;
  }
}
